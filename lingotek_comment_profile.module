<?php

/**
 * @file
 * Implements Drupal-related hooks for the Lingotek Comment Profile module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Render\Element;
use Drupal\lingotek\Entity\LingotekProfile;
use Drupal\lingotek\Lingotek;
use Drupal\lingotek\LingotekProfileInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_lingotek_content_entity_get_profile().
 *
 * If the document being uploaded is a comment, use the profile from the parent.
 */
function lingotek_comment_profile_lingotek_content_entity_get_profile(ContentEntityInterface $entity, LingotekProfileInterface &$profile = NULL, $provide_default = TRUE) {
  if ($entity->getEntityTypeId() === 'comment') {
    /** @var \Drupal\comment\CommentInterface $entity */
    $commented = $entity->getCommentedEntity();
    /** @var \Drupal\lingotek\LingotekConfigurationServiceInterface $lingotek_config */
    $lingotek_config = \Drupal::service('lingotek.configuration');
    // We must prevent a default, or we get into a recursion issue.
    $profile = $lingotek_config->getEntityProfile($commented, FALSE);
  }

}

/**
 * Implements hook_form_alter().
 */
function lingotek_comment_profile_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if ($form_id === 'lingotek.settings_tab_content_form') {
    if (isset($form['parent_details']['list']['entity-comment'])) {
      $children = Element::children($form['parent_details']['list']['entity-comment']['content']['comment']);
      foreach ($children as $bundle_id) {
        $form['parent_details']['list']['entity-comment']['content']['comment'][$bundle_id]['profiles']['#default_value'] = 'manual';
        $form['parent_details']['list']['entity-comment']['content']['comment'][$bundle_id]['profiles']['#disabled'] = TRUE;
        $form['parent_details']['list']['entity-comment']['content']['comment'][$bundle_id]['profiles']['#description'] =
          t("This value cannot be edited and the value shown above won't be applied. The Lingotek translation profile will be inherited from the commented entity.");
      }
    }
  }
}

/**
 * @file
 * Allows comments to inherit Lingotek profile from the commented entity.
 */

/**
 * Implements hook_help().
 */
function lingotek_comment_profile_help($route_name, RouteMatchInterface $arg) {
  switch ($route_name) {
    case 'help.page.lingotek_comment_profile':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Allows comments to inherit the Lingotek profile from the commented entity.') . '</p>';

            // Add a link to the Drupal.org project.
      $output .= '<p>';
      $output .= t('Visit the <a href=":project_link">Lingotek Comment Profile',[
        ':project_link' => 'https://www.drupal.org/project/lingotek_comment_profile'
        ]);
      $output .= '</p>';

      return $output;
  }
}
